<?php

namespace App\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;

class CompanyController extends AbstractController
{
    #[Route('/companies', name: 'company_index' , methods: ['get' , 'head'])]
    public function index(): JsonResponse
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/Api/CompanyController.php',
        ]);
    }

    #[Route('/companies/{id}', name: 'company_find')]
    public function find(int $id): JsonResponse
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/Api/CompanyController.php',
        ]);
    }

    #[Route('/companies', name: 'company_create' , methods: 'post')]
    public function create(): JsonResponse
    {
        return $this->json([
            'message' => 'Welcome to post new controller!',
            'path' => 'src/Controller/Api/CompanyController.php',
        ]);
    }
}
