<?php

namespace App\Controller\Api;

use App\Service\Rest\Interfaces\IUserRestService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;

class UserController extends AbstractController
{

    public function __construct(private readonly IUserRestService $restService)
    {
    }

    #[Route('/users', name: 'user_index', methods: 'get')]
    public function index(Request $request): JsonResponse
    {
        return $this->restService->index($request);
    }

    #[Route('/users/{id}', name: 'user_find')]
    public function find(int $id): JsonResponse
    {
        return $this->json([
            'message' => "Welcome to your {$id} new controller!",
            'path' => 'src/Controller/Api/UserController.php',
        ]);
    }

    #[Route('/users', name: 'user_create', methods: 'post')]
    public function create(): JsonResponse
    {
        return $this->json([
            'message' => 'Welcome to post new controller!',
            'path' => 'src/Controller/Api/UserController.php',
        ]);
    }
}
