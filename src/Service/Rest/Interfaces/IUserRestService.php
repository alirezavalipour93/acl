<?php

namespace App\Service\Rest\Interfaces;

use Symfony\Component\HttpFoundation\Request;

interface IUserRestService
{
    public function index(Request $request);
    public function find(int $id);
    public function create(Request $request);
    public function delete(int $id);
}