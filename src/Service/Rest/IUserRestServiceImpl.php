<?php

namespace App\Service\Rest;

use App\Repository\UserRepository;
use App\Service\Rest\Interfaces\IUserRestService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

readonly class IUserRestServiceImpl implements IUserRestService
{

    public function __construct(private UserRepository         $userRepository,
                                private EntityManagerInterface $entityManager)
    {
    }

    public function index(Request $request): array
    {
        return $this->userRepository->findAll();
    }

    public function find(int $id)
    {
        return $this->userRepository->find($id);
    }

    public function create(Request $request)
    {


        $this->entityManager->persist();
    }

    public function delete(int $id)
    {
        // TODO: Implement delete() method.
    }
}